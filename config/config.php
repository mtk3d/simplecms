<?php
$config['default_view'] = "page";

/*MySQL*/
$config['db_host'] = 'localhost';
$config['db_username'] = 'root';
$config['db_password'] = '123456';
$config['db_name'] = 'SmallCMS';

try
{
	$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $pdo->query('SELECT * FROM `sc_config`');
	foreach($stmt as $row)
	{
		$config[$row['name']] = $row['value'];
	}
	$stmt->closeCursor();
}
catch(PDOException $e)
{
	echo 'Połączenie nie mogło zostać nawiązane: ' . $e->getMessage();
}
<content>
<article>
<h2>Zarządzanie stronami</h2>
<p>
<?php
if(!isset($_GET['operation']) || empty($_GET['operation']))
	{
		echo '<a style="float: left;" href="index.php?view=pages&operation=add">Dodaj stronę</a>';
		echo '<form action="index.php?view=pages&operation=change_home_page" method="post" style="float: right;">';
		echo 'Strona główna:&nbsp;';
		echo '<select name="id">';
		try
		{
			$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$count = 0;
			$stmt = $pdo->query('SELECT * FROM `sc_page`');
			foreach($stmt as $row)
			{
				$short = substr($row['content'], 0, 50).'...';
				if($row['id']==$config['home_page_id'])
				{
					echo '<option value="'.$row['id'].'" selected>'.$row['title'].'</option>';
				}
				else
				{
					echo '<option value="'.$row['id'].'">'.$row['title'].'</option>';
				}
				$count++;
			}
			if($count == 0)
			{
				echo "Brak wpisów";
			}
			$stmt->closeCursor();
		}
		catch(PDOException $e)
		{
			echo 'Połączenie nie mogło zostać nawiązane: ' . $e->getMessage();
		}
		echo '</select>&nbsp;<input type="submit" value="Ustaw" /></form><br /><br />';

		echo '<table width="100%" border="0" cellspacing="0">';
		try
		{
			$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$count = 0;
			$stmt = $pdo->query('SELECT * FROM `sc_page`');
			foreach($stmt as $row)
			{
				$short = substr($row['content'], 0, 50).'...';
				echo '<tr><td>'.$row['title'].'</td><td>'.$short.'</td><td><a href="index.php?view=pages&operation=edit&id='.$row['id'].'">edytuj</a></td><td><a href="index.php?view=pages&operation=delete&id='.$row['id'].'">usuń</a></td></tr>';
				$count++;
			}
			if($count == 0)
			{
				echo "Brak wpisów";
			}
			$stmt->closeCursor();
		}
		catch(PDOException $e)
		{
			echo 'Połączenie nie mogło zostać nawiązane: ' . $e->getMessage();
		}
		echo '</table>';
	}
	else if($_GET['operation']=='delete')
	{
		$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
		$count = $pdo->exec("DELETE FROM sc_page WHERE id = ".$_GET['id']);
		if($count>0)
		{
			header('Location: index.php?view=pages');
		}
		else
		{
			echo "Wpis nie został usunięty";
		}
	}
	else if($_GET['operation']=='add')
	{
		echo '<a href="index.php?view=pages">Wróć</a><br /><br />';

		echo '<select id="image_to_add">';
		if ($handle = opendir('images')) {

    		while (false !== ($entry = readdir($handle))) {
        		if($entry != '.' && $entry != '..')
        		{
        			echo '<option value="images/'.$entry.'">'.$entry.'</option>';
        		}
    		}
    	closedir($handle);
		}
		echo '</select>&nbsp;<input type="button" onclick="addImage();" value="Dodaj obraz" />';

		echo '<form action="index.php?view=pages&operation=add_sql" method="post">';
		echo '<input type="text" name="title" size="70" placeholder="Tytuł" />';
		echo '<textarea id="content_area" rows="15" cols="70" name="content" placeholder="Treść"></textarea><br />';
		echo '<input type="submit" value="Wyślij" />
			</form>
		';
	}
	else if($_GET['operation']=='add_sql')
	{
		$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
		$count = $pdo->exec("INSERT INTO `sc_page` (`id`, `title`, `content`) VALUES (NULL, '".$_POST['title']."', '".$_POST['content']."');");
		if($count>0)
		{
			header('Location: index.php?view=pages');
		}
		else
		{
			echo '<a href="index.php?view=pages">Wróć</a><br /><br />';
			echo "Wpis nie został dodany";
		}
	}
	else if($_GET['operation']=='edit')
	{
		echo '<a href="index.php?view=pages">Wróć</a><br />';

		try
		{
			$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$count = 0;
			$stmt = $pdo->query('SELECT * FROM `sc_page` WHERE id='.$_GET['id'].' LIMIT 1');
			foreach($stmt as $row)
			{
				echo '<select id="image_to_add">';
				if ($handle = opendir('images')) {

    				while (false !== ($entry = readdir($handle))) {
        				if($entry != '.' && $entry != '..')
        				{
        					echo '<option value="images/'.$entry.'">'.$entry.'</option>';
        				}
    				}
    			closedir($handle);
				}
				echo '</select>&nbsp;<input type="button" onclick="addImage();" value="Dodaj obraz" />';

				echo '<form action="index.php?view=pages&operation=edit_sql&id='.$_GET['id'].'" method="post">';
				echo '<input type="text" name="title" size="70" value="'.$row['title'].'" />';
			    echo '<textarea id="content_area" rows="15" cols="70" name="content">'.$row['content'].'</textarea><br />';
		    }
		    $pdo = null;
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    die();
		}

		echo '<input type="submit" value="Wyślij" />
			</form>
		';
	}
	else if($_GET['operation']=='edit_sql')
	{
		$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
		$count = $pdo->exec("UPDATE `sc_page` SET `title` = '".$_POST['title']."', `content` = '".$_POST['content']."' WHERE `sc_page`.`id` = ".$_GET['id'].";");
		if($count>0)
		{
			header('Location: index.php?view=pages');
		}
		else
		{
			echo '<a href="index.php?view=pages">Wróć</a><br /><br />';
			echo "Wpis nie został zmieniony";
		}
	}
	else if($_GET['operation']=='change_home_page')
	{
		$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
		$count = $pdo->exec("UPDATE `sc_config` SET `value` = '".$_POST['id']."' WHERE name = 'home_page_id';");
		if($count>0)
		{
			header('Location: index.php?view=pages');
		}
		else
		{
			echo '<a href="index.php?view=pages">Wróć</a><br /><br />';
			echo "Wpis nie został zmieniony";
		}
	}
?>
</p>
</article>
</content>
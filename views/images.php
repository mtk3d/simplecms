<content>
<article>
	<h2>Obrazy</h2>

<?php
	if(!isset($_GET['operation']) || empty($_GET['operation']))
	{
		echo '<div style="text-align: center;"><a href="index.php?view=images&operation=add_image">Dodaj obraz</a></div><br />';
		if ($handle = opendir('images')) {

    		while (false !== ($entry = readdir($handle))) {
        		if($entry != '.' && $entry != '..')
        		{
        			echo '<div class="image">
        				<div class="img" style="background-image: url(images/'.$entry.');"></div>
						<a href="index.php?view=images&operation=remove&file_name='.$entry.'">usuń</a>
						</div>';
        		}
    		}
    	closedir($handle);
		}
	}
	else if($_GET['operation']=='remove')
	{
		unlink('images/'.$_GET['file_name']);
		header('Location: index.php?view=images');
	}
	else if($_GET['operation']=='add_image')
	{
		echo '<div style="text-align: center;"><a href="index.php?view=images">Wróć</a></div><br />';
		echo '<form style="text-align:center;" action="index.php?view=images&operation=add_image_execute" method="post" enctype="multipart/form-data">
    	<input type="file" name="fileToUpload" id="fileToUpload">
    	<input type="submit" value="Dodaj obraz" name="submit">
		</form>';
	}
	else if($_GET['operation']=='add_image_execute')
	{
		$target_dir = "images/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		    if($check !== false) {
		        echo "Plik - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "Plik nie jest zdjęciem ";
		        $uploadOk = 0;
		    }
		}

		if (file_exists($target_file)) {
		    echo "Plik istnieje";
		    $uploadOk = 0;
		}

		if ($_FILES["fileToUpload"]["size"] > 500000) {
		    echo "Plik jest za duży";
		    $uploadOk = 0;
		}

		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    echo "tylko pliki JPG, JPEG, PNG i GIF.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Nie udało się dodać pliku";
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		        header('Location: index.php?view=images');
		    } else {
		        echo "Nie udało się dodać pliku";
		    }
		}
	}
?>
</article>
</content>
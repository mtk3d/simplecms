<!DOCTYPE html>
<html>
<head>
	<title><?=$config['page_name']; ?></title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="shortcut icon" href="favicon.ico" />
	<script src="js/script.js"></script>
</head>
<body>
	<header>
		<h1><a style="text-decoration: none; color: #000;" href="index.php"><?=$config['page_name']; ?></a></h1>
	</header>
	<main>
<content>
<?php
include('config/config.php');
$page = isset($_GET['id']) ? $_GET['id'] : $config['home_page_id'];
try
{
	$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$count = 0;
	$stmt = $pdo->query('SELECT * FROM `sc_page` WHERE id='.$page.' LIMIT 1');
	foreach($stmt as $row)
	{
		echo '<article>
		<h2>'.$row['title'].'</h2>
		<p>
			'.$row['content'].'
		</p>
		</article>';
		$count++;
	}
	if($count == 0)
	{
		header('Location: 404.php');
	}
	$stmt->closeCursor();
}
catch(PDOException $e)
{
	echo 'Połączenie nie mogło zostać nawiązane: ' . $e->getMessage();
}
?>
</content>
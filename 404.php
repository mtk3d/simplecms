<!DOCTYPE html>
<html>
<head>
	<title>404 Error</title>
</head>
<body>
	<div id="404" style="font-family: Arial; font-size: 25px; text-align: center; width: 250px; box-shadow: 1px 1px 10px #a8a8a8; margin: 0 auto; padding: 50px; color: #6b6b6b;">
		404 Page not found
	</div>
	<script type="text/javascript">
		document.getElementById('404').style.marginTop = (window.innerHeight/2-150) + 'px';
	</script>
</body>
</html>
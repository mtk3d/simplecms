<?php
include('config/config.php');
function checkLogin($login, $password)
	{
		try
		{
			global $config;
			$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
			$pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $pdo->query('SELECT id, username FROM sc_users WHERE username = "'.$login.'" AND password = "'.md5($password).'" LIMIT 1');
			foreach($stmt as $row)
			{
				$id = $row['id'];
				$user = $row['username'];
			}
			if(isset($id))
			{
				return $user;
			}
			else
			{
				return 0;
			}
			
		}
		catch(PDOException $e)
		{
			echo 'Połączenie nie mogło zostać nawiązane. '.$e->getMessage();
		}
	}

	function checkUser($login)
	{
		try
		{
			global $config;
			$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
			$pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$stmt = $pdo->query('SELECT id FROM sc_users WHERE username = "'.$login.'" LIMIT 1');
			foreach($stmt as $row)
			{
				$id = $row['id'];
			}
			if(!empty($id))
			{
				return 1;
			}
			else
			{
				return 0;
			}
			
		}
		catch(PDOException $e)
		{
			echo 'Połączenie nie mogło zostać nawiązane. '.$e->getMessage();
		}
	}

	function register($user, $password)
	{
		if(checkUser($user)!=1)
		{
			try
			{
				global $config;
				$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
				$pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$liczba = $pdo->exec("INSERT INTO `sc_users` (`id`, `username`, `password`) VALUES (NULL, '".$user."', md5('".$password."'))");
				if($liczba>0)
				{
					echo '<center>Użytkownik został dodany<br /><small><small>Za chwilę nastąpi przekierowanie</small></small></center>';
					header('refresh:2; url=index.php', true, 303);
				}
				else
				{
					echo '<center>Wystąpił błąd podczas dodawania<br /> <small>Za chwilę nastąpi przekierowanie</small></center>';
					header('refresh:2; url=index.php', true, 303);
				}
			}
			catch(PDOException $e)
			{
				echo 'Połączenie nie mogło zostać nawiązane. '.$e->getMessage();
			}
		}
		else
		{
			echo '<center>Użytkownik o podanej nazwie już istnieje<br /><small><small>Za chwilę nastąpi przekierowanie</small></small></center>';
			header('refresh:2; url=index.php?action=register', true, 303);
		}

	}

	if(isset($_SESSION['login']))
	{
		if(!isset($_GET['action']))
		{
		echo 'Zalogowany jako <big>'.$_SESSION['login'].'</big><br />';
		echo '<small><a href="index.php?view=pages">Zarządzaj stronami</a><br />';
		echo '<a href="index.php?view=images">Zarządzaj obrazami</a><br />';
		echo '<a href="index.php?action=logout">Wyloguj</a></small>';
		}
		else if($_GET['action']=='logout')
		{
			session_unset();
			echo '<center>Wylogowano <br /> <small>Za chwilę nastąpi przekierowanie</small></center>';
			header('refresh:2; url=index.php', true, 303);
		}
	}
	else
	{
		if(!isset($_GET['action']))
		{
		echo '
		<div style="text-align: center;">
			<form method="post" action="index.php?action=login">
				<input type="text" name="login" placeholder="Login" style="margin-bottom: 5px;" /><br />
				<input type="password" name="password" placeholder="Password" style="margin-bottom: 5px;" /><br />
				<input type="submit" value="Login" />
			</form>
			<small><a href="index.php?action=register">Zarejestruj</a></center></small>
		</div>
		';
		}
		else if($_GET['action']=='login')
		{
			if(isset($_POST['login']) && isset($_POST['password']) && !empty($_POST['login']) && !empty($_POST['password']))
			{
				$check = checkLogin($_POST['login'], $_POST['password']);
				if($check===0)
				{
					echo '<center>Błędny login lub hasło <br /> <small>Za chwilę nastąpi przekierowanie</small></center>';
					header('refresh:2; url=index.php', true, 303);
				}
				else
				{
					echo '<center>Zalogowano <br /> <small>Za chwilę nastąpi przekierowanie</small></center>';
					$_SESSION['login']=$check;

					header('refresh:2; url=index.php', true, 303);
				}
			}
			else
			{
				echo '<center>Wypełnij wszystkie pola <br /> <small>Za chwilę nastąpi przekierowanie</small></center>';
				header('refresh:2; url=index.php', true, 303);
			}
		}
		else if($_GET['action']=='register')
		{
		echo '<div style="text-align: center;">
			<h1>Rejestracja</h1>
			<form method="post" action="index.php?action=register_sql">
				<input type="text" name="login" placeholder="Login" style="margin-bottom: 5px;" /><br />
				<input type="password" name="password" placeholder="Password" style="margin-bottom: 5px;" /><br />
				<input type="submit" value="Zarejestruj" />
			</form>
			<small><a href="index.php">Powrót</a></small></center>
		</div>';
		}
		else if($_GET['action']=='register_sql')
		{
			if(isset($_POST['login']) && isset($_POST['password']) && !empty($_POST['login']) && !empty($_POST['password']))
			{
				register($_POST['login'], $_POST['password']);
			}
			else
			{
				echo '<center>Wypełnij wszystkie pola <br /> <small>Za chwilę nastąpi przekierowanie</small></center>';
				header('refresh:2; url=index.php?action=register', true, 303);
			}
		}
	}
?>
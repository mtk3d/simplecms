<nav>
	<?php
	include('config/config.php');
	$page = isset($_GET['page']) ? $_GET['page'] : $config['home_page_id'];
	try
	{
		$pdo = new PDO('mysql:host='.$config['db_host'].';dbname='.$config['db_name'].'', $config['db_username'], $config['db_password']);
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $pdo->query('SELECT * FROM `sc_page`');
		foreach($stmt as $row)
		{
			echo '<a href="index.php?view=page&id='.$row['id'].'">'.$row['title'].'</a><br />';
		}
		$stmt->closeCursor();
	}
	catch(PDOException $e)
	{
		echo 'Połączenie nie mogło zostać nawiązane: ' . $e->getMessage();
	}
	?>
</nav>
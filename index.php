<?php
session_start();
require('config/config.php');

$url['view'] = empty($_GET['view']) ? $config['default_view'] : $_GET['view'];
$url['method'] = empty($_GET['action']) ? 'index' : $_GET['action'];
$url['parameter'] = empty($_GET['parameter']) ? NULL : $_GET['parameter'];

if(file_exists('views/'.$url['view'].'.php'))
{
	include('views/header.php');
	include('views/'.$url['view'].'.php');
	include('views/sidebar.php');
	include('views/footer.php');
}
else
{
	header('Location: 404.php');
}
?>
